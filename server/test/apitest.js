process.env.NODE_ENV = 'test';
let db = require("../app/config/db.config.js");
let Post = db.posts;
let PostMeta = db.postMetas;
let chai = require('chai');
let chaiHttp = require('chai-http');
var should = chai.should();
chai.use(chaiHttp);
let server = require('../app');
let post_id = "";

//Our parent block

describe('All Posts', () => {
	describe('/GET posts', () => {
		it('it should GET all the posts', (done) => {
			chai.request(server)
			.get('/posts')
			.end((err, res) => {
				if(err){
					console.log('all posts error', err);
				}
				(res).should.have.status(200);
				(res.body).should.be.a('object');
				(res.body.posts.length).should.be.eql(0);
				done();
			});
		});
	});
});

describe('Insert Post', () => {
	describe('/POST posts', () => {
		it('it should send parameters to : /posts POST', (done) => {
			chai.request(server)
			.post('/posts')
			.set('Content-Type', 'multipart/form-data;')
			.field('title', 'TEST A')
			.field('description', 'TEST ABCD')
			.field('author', 'TEST A')
			.attach('image','./test/mevn.png')
			.end((err, res) => {
				(res).should.have.status(200);
				(res.body).should.be.a('object');
				(res.body.success).should.be.eql(true);
				done();
			});
		});
	});
});

describe('Update Post', () => {
	describe('/PUT posts', () => {
		it(`it should send parameters to : /posts/:id PUT`, async () => {
			await chai.request(server)
			.post('/posts')
			.set('Content-Type', 'multipart/form-data;')
			.field('title', 'TEST A')
			.field('description', 'TEST ABCD')
			.field('author', 'TEST A')
			.attach('image','./test/mevn.png')
			.then((res, err) => {
				if(err){
					console.log('update error', err);
				}
				post_id = res.body.post_id;
			});
			chai.request(server)
			.put(`/posts/${post_id}`)
			.set('Content-Type', 'multipart/form-data;')
			.field('title', 'TEST B')
			.field('description', 'TEST ABCD')
			.field('author', 'TEST A')
			.attach('image','./test/mevn.png')
			.end((err, res) => {
				if(err){
					console.log('update error', err);
				}
				(res).should.have.status(200);
				(res.body).should.be.a('object');
				(res.body.success).should.be.eql(true);
				// done();
			});
		});
	});
});

describe('DELETE Post', () => {
	describe('/DELETE posts', () => {
		it(`it should send parameters to : /posts/${post_id} DELETE`, async () => {
			await chai.request(server)
			.post('/posts')
			.set('Content-Type', 'multipart/form-data;')
			.field('title', 'TEST A')
			.field('description', 'TEST ABCD')
			.field('author', 'TEST A')
			.attach('image','./test/mevn.png')
			.then((res, err) => {
				if(err){
					console.log('delete error', err);
				}
				post_id = res.body.post_id;
			});
			chai.request(server)
			.delete(`/posts/${post_id}`)
			.end((err, res) => {
				if(err){
					console.log('delete error', err);
				}
				(res).should.have.status(200);
				(res.body).should.be.a('object');
				(res.body.success).should.be.eql(true);
				// done();
			});
		});
	});
});

// in your beforeEach hook, pass 'done' to 'clear_the_db'
beforeEach(function (done){
  clear_the_post_table(done);
});

function clear_the_post_table(doneCb) {
  Post.deleteMany({}, function(err){
    if(!err) {
      console.log('Post collections removed');
    } else {
      console.log('Error is= '+err);
    }
    clear_the_postMeta_table(doneCb);
  });
}

function clear_the_postMeta_table(doneCb) {
  PostMeta.deleteMany({}, function(err){
    if(!err) {
      console.log('Post Meta collections removed');
    } else {
      console.log('Error is= '+err);
    }
    // call the Mocha done() callback function 
    doneCb();
  });
}