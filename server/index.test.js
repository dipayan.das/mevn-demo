process.env.NODE_ENV = 'test';
const request = require('supertest');
const app = require('../app.js');

describe('GET /posts', function() {
  it('return json response', function() {
    return request(app)
      .get('/posts')
      .expect(200)
      .expect('Content-Type',/json/);
      done();
  });
})