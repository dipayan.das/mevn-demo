const express = require('express');
const multer = require('multer');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const path = require('path');

const app = express();
app.use(morgan('combined'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
app.use(cors());
app.use(express.static(path.join(__dirname, "/public")));
require("./app/route/post.route.js")(app, multer);

const port = 8081;
//Create server
var server = app.listen(port, function(){
	console.log(`The App Is Running on http://localhost:${port}`);
});

module.exports = server;