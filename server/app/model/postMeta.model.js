module.exports = (mongoose, Mongoose, Post) => {
	const PostMeta = new Mongoose.Schema({
		author: String,
		image: String,
		post_id: {type: Mongoose.Schema.Types.ObjectId, ref: "Post"}
	}, {
		timestamps: true
	});
	return Mongoose.model("PostMeta", PostMeta);
}