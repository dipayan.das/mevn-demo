module.exports = (mongoose, Mongoose) => {
	const Post = new Mongoose.Schema({
		title: String,
		description: String
	}, {
        timestamps: true
    });
	return Mongoose.model("Post", Post);
}