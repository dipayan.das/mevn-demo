//mongodb://localhost:27017/posts
const env = {
  database: "posts-test",
  host: "localhost",
  port: "27017",
  dialect: "mongodb"
};

module.exports = env;
