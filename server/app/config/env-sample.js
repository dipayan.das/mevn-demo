const env = {
  database: "demo2",
  username: "postgres",
  password: "root",
  host: "localhost",
  dialect: "postgres",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  },
  aws_access_key_id: "xxxxxxxxxxxxx",
  aws_secret_access_key: "xxxxxxxxxxxxx",
  s3bucketname: "my-s3-bucket"
};

module.exports = env;
