let env;
if(process.env.NODE_ENV == "test"){
	env = require('./env-test.js');
}else{
	env = require('./env.js');
}

const Mongoose = require('mongoose');
const mongoose = Mongoose.connect(env.dialect+'://'+env.host+':'+env.port+'/'+env.database, { useNewUrlParser:true, useUnifiedTopology: true }).connection;
Mongoose.set('useNewUrlParser', true);
Mongoose.set('useFindAndModify', false);
Mongoose.set('useCreateIndex', true);
Mongoose.set('useUnifiedTopology', true);

const db = {};

db.Mongoose = Mongoose;
db.mongoose = mongoose;

//Models/Tables
db.posts = require('./../model/post.model.js')(mongoose, Mongoose);
db.postMetas = require('./../model/postMeta.model.js')(mongoose, Mongoose, db.posts);

module.exports = db;