const {update,create,findAll,findById,deletePost} = require("./../controller/post.controller.js");

module.exports = function(app, multer) {
	var storage = multer.diskStorage({
		destination: function(req, file, callback) {
			callback(null, "./public/uploads/postImages");
		},
		filename: function(req, file, callback) {
			callback(null, file.fieldname + "_" + Date.now() + "_" + file.originalname.replace(" ", "_"));
		}
	});
	var upload = multer({storage: storage});

	//Create post
	app.post('/posts', upload.single('image'), async (req, res) => {
		let data = req.body;
		if(req.file){
			data.image_url = `http://localhost:8081/uploads/postImages/`+req.file.filename;
		}else{
			data.image_url = `https://www.writingpapersucks.com/wp-content/themes/writingpapersucks/images/default-image.png`;
		}
		await create(data, (response) => {
			if(response.msg == "error"){
				res.send({
					error:true,
					message:response.msg
				});		
			}
			res.send({
				success:true,
				post_id:response.post_id
			});
		});
	});

	//Retrieve all posts
	app.get('/posts', findAll);

	//Retriveve a single post by ID
	app.get('/post/:id', findById);

	//Update a post with ID
	app.put('/posts/:id', upload.single('image'), update);

	//Delete a post with ID
	app.delete('/posts/:id', deletePost);

}