const db = require("../config/db.config.js");
const Post = db.posts;
const PostMeta = db.postMetas;
const fs = require('fs');
const path = require('path');

//Create a post
exports.create = (data, callback) => {
	//Save to MongoDB
	const addPost = new Post({
		title: data.title,
		description: data.description
	});
	addPost.save(async function(err){
		try {
			if(err){
				throw (err);
			}
			const addPostMeta = new PostMeta({
				author: data.author,
				image: data.image_url,
				post_id: addPost._id
			});
			await addPostMeta.save(function(err){
				if(err){
					throw (err);
				}
			})
			callback({success: true, post_id: addPost._id});
			// res.json({success: true});
		} catch(err) {
			console.log(err);
			callback({msg: "error", details: err});
			// res.status(500).json({ msg: "error", details: err });
		}
	});
}

//Fetch all posts
exports.findAll = (req, res) => {
	let postdata = [];
	Post.find({}, 'title description').sort({_id:-1}).then(async (posts) => {
		if(posts.length > 0){
			let i = 0;
			for(const post of posts){
				await PostMeta.findOne({post_id:post._id}, 'author image').then(postmeta => {
					if(postmeta != null){
						let postMetaData = { author: postmeta.author, image:postmeta.image };
						postdata[i] = {...posts[i]._doc, ...postMetaData};
					}else{
						postdata[i] = posts[i];
					}
					i++;
				}).catch(error => {
					console.log(error);
					res.status(500).json({ msg: "error", details: error });
				})
			}
			//Send all posts to client
			res.json({
				posts: postdata 
			});
		}else{
			res.json({
				posts: postdata 
			});
		}
	}).catch(err => {
		console.log(err);
		res.status(500).json({ msg: "error", details: err });
	});
}

//Fetch a single post
exports.findById = (req, res) => {
	try {
		Post.findById(req.params.id, 'title description', async function(err, post){
			if(err){
				throw (err);	
			}
			let postdata = {...post._doc}
			await PostMeta.findOne({post_id:req.params.id}, 'author image').then(postmeta => {
				if(postmeta != null && Object.keys(postmeta).length !== 0){
					postdata = {}
					postdata = {...post._doc, ...postmeta._doc}
				}
			}).catch(error => {
				console.log(error);
				res.status(500).json({ msg: "error", details: error });
			});
			res.json(postdata);
		});
	} catch(e) {
		console.log(e);
		res.status(500).json({ msg: "error", details: e });
	}
}

//Update a post
exports.update = async (req, res) => {
	return new Promise(async (resolve, reject) => {
		let data = req.body;
		let post_data = {
			'title': data.title,
			'description': data.description
		};
		await Post.findByIdAndUpdate(req.params.id, post_data, {new:1}).then(async (post) => {
			await PostMeta.findOne({post_id: req.params.id}).then(async (postmeta) => {
				if(postmeta!=null && Object.keys(postmeta).length!==0){
					let postmeta_data = {
						'author': req.body.author,
						'image':  (typeof req.file !== 'undefined')?`http://localhost:8081/uploads/postImages/${req.file.filename}`:postmeta.image
					};
					if(typeof postmeta.image !== 'undefined' && typeof req.file !== 'undefined'){
						imageURL = postmeta.image.split('/');
						imageFileName = imageURL.pop();
						console.log('imageFileName', imageFileName);
						let file = path.join(__dirname, `../../public/uploads/postImages/${imageFileName}`);
						fs.access(file, (err) => {
							if(!err){
								fs.unlinkSync(path.join(__dirname, `../../public/uploads/postImages/${imageFileName}`), (errImg) => {
									if(errImg){
										reject(errImg);
									}
								});
							}
						});
					}
					await PostMeta.findOneAndUpdate({post_id: req.params.id}, postmeta_data).then( () => {
						resolve(true);
					}).catch(error => {
						reject(error);
					});
				}else{
					let postmeta_data_create = {
						'post_id': req.params.id,
						'author': req.body.author,
						'image':  (typeof req.file !== 'undefined')?`http://localhost:8081/uploads/postImages/${req.file.filename}`:'https://www.writingpapersucks.com/wp-content/themes/writingpapersucks/images/default-image.png'
					};
					await PostMeta.create(postmeta_data_create).then( () => {
						resolve(true);
					}).catch(error => {
						reject(error);
					});
				}
			}).catch(error => {
				reject(error);
			});
		}).catch(error => {
			reject(error);
		});
	}).then((status) => {
		res.json({success: status});
	}).catch(error => {
		console.log(error);
	});
}

//Delete a post
exports.deletePost = async (req, res) => {
	await PostMeta.findOne({post_id:req.params.id}, 'author image', async function(err2, postmeta){
		if(err2){
			res.status(500).json({ msg: "error", details: err2 });
		}
		if(postmeta != null && Object.keys(postmeta).length !== 0){
			if(typeof postmeta.image !== 'undefined'){
				imageURL = postmeta.image.split('/');
				imageFileName = imageURL.pop();
				fs.unlinkSync(path.join(__dirname, `../../public/uploads/postImages/${imageFileName}`), (errImg) => {
					if(errImg){
						res.status(500).json({ msg: "error", details: errImg });
					}
				});
			}
		}
	});
	Post.remove({ _id: req.params.id }, async function(err){
		if(err){
			res.status(500).json({ msg: "error", details: err });
		}
		await PostMeta.remove({post_id: req.params.id}, function(error){
			if(error){
				res.status(500).json({ msg: "error", details: error });
			}
		})
		res.json({
			success: true
		});
	});
}