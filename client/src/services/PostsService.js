import Api from '@/services/Api'

export default {
  fetchPosts () {
    return Api().get('posts')
  },
  addPost (params) {
    return Api().post('posts', params)
  },
  updatePost (id, params) {
    return Api().put('posts/' + id, params)
  },
  getPost (params) {
    return Api().get('post/' + params.id)
  },
  deletePost (id) {
    return Api().delete('posts/' + id)
  }
}
